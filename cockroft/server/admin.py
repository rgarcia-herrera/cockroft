#from django.contrib import admin
from django.contrib.gis import admin
from server import models
from django.contrib.gis.geos.point import Point

@admin.register(models.Venue)
class VenueAdmin(admin.ModelAdmin):
    model = models.Venue

    # list_display = ['name', 'owner', 'default_government' ]
    # search_fields = [
    #                  'owner__user__first_name',
    #                  'owner__user__last_name',
    #                  'owner__user__email',
    #                  'name']
    # list_filter = ['default_government', ]
    # inlines = [MembershipInline, IssueInline ]


@admin.register(models.Performer)
class PerformerAdmin(admin.ModelAdmin):
    model = models.Performer


@admin.register(models.Patron)
class PatronAdmin(admin.ModelAdmin):
    model = models.Patron


@admin.register(models.Show)
class ShowAdmin(admin.ModelAdmin):
    model = models.Show
