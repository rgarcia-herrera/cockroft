from django.db import models
from django.contrib.gis.db.models import PointField
from django.contrib.auth.models import User

class Venue(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    capacity = models.IntegerField(default=1)
    point = PointField(null=True, blank=True)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)


class Performer(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)

    title = models.CharField(max_length=20)
    staff_size = models.IntegerField(default=1)
    description = models.TextField()


class Patron(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE)



class Show(models.Model):
    date = models.DateTimeField()
    venue = models.ForeignKey(Venue,
                              on_delete=models.CASCADE)
    performer = models.ForeignKey(Performer,
                                    on_delete=models.CASCADE)
    status = models.CharField(max_length=20)


class Ticket(models.Model):
    show = models.ForeignKey(Show,
                                on_delete=models.CASCADE)
    patron = models.ForeignKey(Patron,
                                on_delete=models.CASCADE)
